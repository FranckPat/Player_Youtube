<?php
    session_start();
  if (isset($_SESSION['id'])){
?>
<!DOCTYPE html>
    <html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>YOUMIX</title>
    <script type="text/javascript" src="script/Index.js"></script>
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  </head>
  <body>
      <a href="./connexion_session/session_deconnect.php"><button>Déconnection</button></a>
      <div class="player">
        <h1 class='title'>YOUMIX :</h1>
      </div>
      <div class="container_movie">
        <div class="movieContainer">
          <iframe width="560" height="315" src="<?php
            $linked = substr($_POST['link'], 32,11); //Récupération de l'adresse URL de YouTube + découpe de cette adresse entre une entrée précise
            echo "https://www.youtube.com/embed/$linked?rel=0&autoplay=1";///Ajout du lien au vrai lien de l'iframe?>" style="margin:auto;" id="iframe" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="container_playlist">
        <?php
              include('Connexion/ADP.php');
              $videos = getVideos();
            foreach($videos as $value){
          ?>
          <form action="Index.php" method="POST">
            <button type="submit" class="list-group-item list-group-item-action"><?php echo $value['title'];?></button>
            <input type="hidden" name="link" value="<?php $texte=$value['texte']; echo $texte;?>">
          </form>

          <?php
            }
          ?>
        </div>
        <br>
        <div class="search">
          <form action="Insertion.php" method="POST" class="search">
            <input type="texte" name="texte" class="texte" required>
            <br><input  type="image" src="fleche_haute.jpg" class="image" value="submit">
          </form>
        </div>
          <?php echo "<font color=black><h5 style='color:white;text-decoration:underline;'><center>&copy tous droits réservé</center></h5></font>";?>
  </body>
</html>
<?php
  }else{
    echo '<u>Connectez-vous avec ceci :</u><br><a href="Inscription/Inscription.php">Inscription aux site</a>';
  }
?>
