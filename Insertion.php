<?php
  session_start();
  try{
    $text = $_POST['texte'];
    $textid = substr($text,32,11);
    include_once('./Connexion/ConnexionDB.php');
    $db = BaseDonnee::connection();
    $verify = $db->prepare('SELECT * FROM youmix_ajout WHERE texte=:texte');
    $verify->execute(array(
        'texte'=>$text
      ));
    $count = $verify->rowCount();
      if($count == 1){
        echo 'URL déjà existante';
        header('refresh=3;URL=../Index.php');
        exit;
      }else{
        $key = "AIzaSyDeKworbtb5y_uWpsMF-N2vbVBs0wU3KGc";
        $array = json_decode(file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=id,snippet&id=$textid&key=$key"));
        $title = $array->items[0]->snippet->title;
        $txt = $db->prepare('INSERT INTO youmix_ajout (texte, title, id_user) VALUES (:texte, :title, :id_user)');
        $txt->execute([
          'texte'=>$text,
          'title'=>$title,
          'id_user'=>$_SESSION['id'],
        ]);
          header('Location: ' . $_SERVER['HTTP_REFERER']);
      }
  }
  catch (PDOException $e){
    die('Échec lors de la connexion');
  }
?>
