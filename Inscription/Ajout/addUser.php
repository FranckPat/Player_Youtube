<?php
  try{
        $addUser = $_POST['NameUser'];
        $Email = $_POST['Email'];
        $passUser = $_POST['PassUser'];
        $passwordHash = hash("sha256",$passUser);// Valide
        htmlspecialchars($_POST['NameUser']);
        htmlspecialchars($_POST['Email']);
        htmlspecialchars($_POST['PassUser']);
        include_once('../../Connexion/ConnexionDB.php');
        $connect = BaseDonnee::connection();
        $req=$connect->prepare('INSERT INTO utilisateurs(`utilisateurs`,`mot_de_passe`,`eMail`) VALUES(:utilisateurs,:mot_de_passe,:eMail)');
        $req->execute([
            'utilisateurs'=> $addUser,
            'mot_de_passe'=>$passwordHash,
            'eMail'=>$Email
        ]);
        if (filter_var($Email, FILTER_VALIDATE_EMAIL)){
            header('Location:../Inscription.php');
        } else {
            // Non valide
            header('Location:../Inscription.php?error=Veuillez renseignez votre E-mail');
        }
        session_start();
        session_destroy();

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    catch (PDOException $e){
    die('Échec lors de l\'inscription essayez avec une autre adresse e-mail :<br><a href="../Inscription.php">Cliquez ici...</a>');
    }
?>